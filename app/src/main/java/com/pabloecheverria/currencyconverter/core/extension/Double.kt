package com.pabloecheverria.currencyconverter.core.extension

fun Double.format(digits: Int) = "%.${digits}f".format(this)