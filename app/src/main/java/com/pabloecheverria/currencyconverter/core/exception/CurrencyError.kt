package com.pabloecheverria.currencyconverter.core.exception

sealed class CurrencyError {
    object NetworkConnection : CurrencyError()
    object ServerError : CurrencyError()
}