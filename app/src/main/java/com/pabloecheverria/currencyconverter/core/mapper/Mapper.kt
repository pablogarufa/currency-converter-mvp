package com.pabloecheverria.currencyconverter.core.mapper

interface Mapper<I, O> {
    fun map(input: I): O
}