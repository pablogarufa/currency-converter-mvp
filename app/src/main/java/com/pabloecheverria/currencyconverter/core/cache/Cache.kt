package com.pabloecheverria.currencyconverter.core.cache

interface Cache<K, T> {

    fun get(key: K): T?

    fun set(key: K, value: T)
}