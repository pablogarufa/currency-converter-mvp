package com.pabloecheverria.currencyconverter.core.di

import android.content.Context.MODE_PRIVATE
import com.google.gson.Gson
import com.pabloecheverria.currencyconverter.core.cache.Cache
import com.pabloecheverria.currencyconverter.core.coroutines.AsyncExecutor
import com.pabloecheverria.currencyconverter.core.mapper.Mapper
import com.pabloecheverria.currencyconverter.feature.converter.data.CurrencyRepositoryImpl
import com.pabloecheverria.currencyconverter.feature.converter.data.RatesMapper
import com.pabloecheverria.currencyconverter.feature.converter.data.cache.MemoryCache
import com.pabloecheverria.currencyconverter.feature.converter.data.cache.SharedPrefsCache
import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import com.pabloecheverria.currencyconverter.feature.converter.data.net.CurrencyService
import com.pabloecheverria.currencyconverter.feature.converter.domain.ConvertCurrency
import com.pabloecheverria.currencyconverter.feature.converter.domain.CurrencyRepository
import com.pabloecheverria.currencyconverter.feature.converter.domain.GetCurrencyRates
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates
import com.pabloecheverria.currencyconverter.feature.converter.presentation.CurrenciesPresenter
import okhttp3.OkHttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


val currenciesModule = module {
    factory { AsyncExecutor() }
    factory { GetCurrencyRates(get()) }
    factory { ConvertCurrency(get()) }
    factory { (view: CurrenciesPresenter.View) ->
        CurrenciesPresenter(view, get(), get(), get())
    }
}

val repositoryModule = module {
    single { Gson() }
    single { androidContext().getSharedPreferences(SHARES_PREFS, MODE_PRIVATE) }
    single(named("prefs_cache")) { SharedPrefsCache(get(), get()) as Cache<String, Rates> }
    single(named("memory_cache")) { MemoryCache(hashMapOf()) as Cache<String, Rates> }
    single { RatesMapper() as Mapper<RatesDTO, Rates> }
    single {
        CurrencyRepositoryImpl(
            get(),
            get(named("memory_cache")),
            get(named("prefs_cache")),
            get()
        ) as CurrencyRepository
    }
}

val networkModule = module {
    single { retrofitInstance }
    single { CurrencyService(get()) }
}

val retrofitInstance: Retrofit by lazy {
    Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

val testNetworkModule = module {
    single { testRetrofitInstance }
    single { CurrencyService(get()) }
}

val testRetrofitInstance: Retrofit by lazy {
    Retrofit.Builder()
        .baseUrl(TEST_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
}

val okHttpClient: OkHttpClient by lazy { OkHttpClient() }

const val BASE_URL = "https://api.exchangeratesapi.io/"
const val TEST_URL = "http://localhost:8080/"
const val SHARES_PREFS = "currencies_prefs"