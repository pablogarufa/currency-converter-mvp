package com.pabloecheverria.currencyconverter.core.coroutines

import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main

class AsyncExecutor(private val dispatcher: CoroutineDispatcher = IO) {

    private val job = Job()
    private val scope = CoroutineScope(job + Main)

    fun <Result> enqueueAndDispatch(
        bgFunction: suspend () -> Result,
        mainFunction: (Result) -> Unit
    ) {
        scope.launch {
            val result = withContext(dispatcher) {
                bgFunction.invoke()
            }
            if (isActive) {
                mainFunction(result)
            }
        }
    }

    fun dispose() {
        job.cancel()
    }
}