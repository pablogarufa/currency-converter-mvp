package com.pabloecheverria.currencyconverter.feature.converter.domain

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.pabloecheverria.currencyconverter.feature.converter.domain.DomainError.GeneralError

class GetCurrencyRates(private val repository: CurrencyRepository) :
        (String, Double) -> Either<DomainError, Map<String, Double>> {

    override fun invoke(
        baseCurrency: String,
        amount: Double
    ): Either<DomainError, Map<String, Double>> {
        return repository.getCurrencyRates(baseCurrency)
            .fold({
                Left(GeneralError)
            }, { rates ->
                Right(rates.rates.mapValues { it.value * amount })
            })
    }
}