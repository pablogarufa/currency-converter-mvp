package com.pabloecheverria.currencyconverter.feature.converter.presentation

import com.pabloecheverria.currencyconverter.core.coroutines.AsyncExecutor
import com.pabloecheverria.currencyconverter.core.extension.format
import com.pabloecheverria.currencyconverter.feature.converter.domain.ConvertCurrency
import com.pabloecheverria.currencyconverter.feature.converter.domain.GetCurrencyRates

class CurrenciesPresenter(
    private val view: View,
    private val executor: AsyncExecutor,
    private val getCurrencyRates: GetCurrencyRates,
    private val convertCurrency: ConvertCurrency
) {

    fun convert(fromCurrency: String, toCurrency: String, amount: Double) {
        TODO()
    }

    fun convertAll(baseCurrency: String, amount: Double) {
        view.showLoading()
        executor.enqueueAndDispatch(
            { getCurrencyRates.invoke(baseCurrency, amount) },
            { result ->
                view.hideLoading()
                result.fold({
                    view.showGeneralError()
                }, { rates ->
                    view.showConversion(rates.mapValues { it.value.format(1) })
                })
            })
    }

    fun dispose() = executor.dispose()

    interface View {
        fun showLoading()
        fun hideLoading()
        fun showGeneralError()
        fun showConversion(conversionResult: String)
        fun showConversion(results: Map<String, String>)
    }
}