package com.pabloecheverria.currencyconverter.feature.converter.data.net

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.pabloecheverria.currencyconverter.core.exception.CurrencyError
import com.pabloecheverria.currencyconverter.core.exception.CurrencyError.ServerError
import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import retrofit2.Retrofit

class CurrencyService(private val retrofit: Retrofit) {

    private val service = lazy { retrofit.create(CurrencyApi::class.java) }

    fun currencies(baseCurrency: String): Either<CurrencyError, RatesDTO> {
        return try {
            val response = service.value.currencies(baseCurrency).execute()
            when (response.isSuccessful) {
                true -> response.body()?.let { Right(it) } ?: Left(ServerError)
                false -> Left(ServerError)
            }
        } catch (exception: Throwable) {
            Left(ServerError)
        }
    }
}