package com.pabloecheverria.currencyconverter.feature.converter.data

import arrow.core.Either
import arrow.core.Left
import arrow.core.Right
import com.pabloecheverria.currencyconverter.core.cache.Cache
import com.pabloecheverria.currencyconverter.core.mapper.Mapper
import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import com.pabloecheverria.currencyconverter.feature.converter.data.net.CurrencyService
import com.pabloecheverria.currencyconverter.feature.converter.domain.CurrencyRepository
import com.pabloecheverria.currencyconverter.feature.converter.domain.DomainError
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates

class CurrencyRepositoryImpl(
    private val service: CurrencyService,
    private val memoryCache: Cache<String, Rates>,
    private val sharedPrefsCache: Cache<String, Rates>,
    private val ratesMapper: Mapper<RatesDTO, Rates>
) : CurrencyRepository {

    override fun getCurrencyRates(baseCurrency: String): Either<DomainError, Rates> {
        return findInMemoryCache(baseCurrency)?.let {
            Right(it)
        } ?: findInPrefsCache(baseCurrency)?.let {
            memoryCache.set(baseCurrency, it)
            Right(it)
        } ?: findInNetwork(baseCurrency)
    }

    private fun findInMemoryCache(baseCurrency: String) = memoryCache.get(baseCurrency)

    private fun findInPrefsCache(baseCurrency: String) = sharedPrefsCache.get(baseCurrency)

    private fun findInNetwork(baseCurrency: String): Either<DomainError, Rates> {
        return service.currencies(baseCurrency).fold({
            Left(DomainError.GeneralError)
        }, {
            memoryCache.set(baseCurrency, ratesMapper.map(it))
            sharedPrefsCache.set(baseCurrency, ratesMapper.map(it))
            Right(Rates(baseCurrency, it.rates))
        })
    }

    override fun convertCurrency(
        fromCurrency: String,
        toCurrency: String,
        amount: Double
    ): Either<DomainError, Double> {
        TODO()
    }
}