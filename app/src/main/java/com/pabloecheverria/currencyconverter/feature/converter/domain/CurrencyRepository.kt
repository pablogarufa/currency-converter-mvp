package com.pabloecheverria.currencyconverter.feature.converter.domain

import arrow.core.Either

interface CurrencyRepository {

    fun getCurrencyRates(baseCurrency: String): Either<DomainError, Rates>

    fun convertCurrency(
        fromCurrency: String,
        toCurrency: String,
        amount: Double
    ): Either<DomainError, Double>
}