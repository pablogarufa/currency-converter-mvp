package com.pabloecheverria.currencyconverter.feature.converter.domain

data class Rates(val baseCurrency: String, val rates: Map<String, Double>)