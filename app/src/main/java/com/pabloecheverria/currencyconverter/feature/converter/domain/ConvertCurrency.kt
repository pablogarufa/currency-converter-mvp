package com.pabloecheverria.currencyconverter.feature.converter.domain

import arrow.core.Either

class ConvertCurrency(private val repository: CurrencyRepository) :
        (String, String, Double) -> Either<Error, Double> {

    override fun invoke(
        fromCurrency: String,
        toCurrency: String,
        amount: Double
    ): Either<Error, Double> {
        TODO()
    }
}