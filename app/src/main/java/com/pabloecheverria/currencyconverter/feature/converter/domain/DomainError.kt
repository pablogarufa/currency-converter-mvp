package com.pabloecheverria.currencyconverter.feature.converter.domain

sealed class DomainError {
    object GeneralError : DomainError()
}