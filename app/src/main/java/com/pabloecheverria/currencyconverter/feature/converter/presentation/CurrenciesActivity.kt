package com.pabloecheverria.currencyconverter.feature.converter.presentation

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.pabloecheverria.currencyconverter.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class CurrenciesActivity : AppCompatActivity(), CurrenciesPresenter.View {

    private val presenter: CurrenciesPresenter by inject { parametersOf(this) }

    private val twDollar = TextWatcherWow { convertAll(ISO_DOLLAR, it.toDouble()) }
    private val twEuro = TextWatcherWow { convertAll(ISO_EURO, it.toDouble()) }
    private val twPound = TextWatcherWow { convertAll(ISO_POUND, it.toDouble()) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpListeners()
    }

    private fun setUpListeners() {
        dollar.setOnClickListener { dollar.selectAll() }
        euro.setOnClickListener { euro.selectAll() }
        pound.setOnClickListener { pound.selectAll() }

        dollar.addTextChangedListener(twDollar)
        euro.addTextChangedListener(twEuro)
        pound.addTextChangedListener(twPound)
    }

    private fun removeTWListeners() {
        dollar.removeTextChangedListener(twDollar)
        euro.removeTextChangedListener(twEuro)
        pound.removeTextChangedListener(twPound)
    }

    private fun convertAll(baseCurrency: String, amount: Double) =
        presenter.convertAll(baseCurrency, amount)

    override fun showLoading() {
    }

    override fun hideLoading() {
    }

    override fun showGeneralError() {
        removeTWListeners()

        dollar.setText(ZERO)
        euro.setText(ZERO)
        pound.setText(ZERO)

        setUpListeners()
    }

    override fun showConversion(conversionResult: String) {
    }

    override fun showConversion(results: Map<String, String>) {
        removeTWListeners()

        results[ISO_DOLLAR]?.let { dollar.setText(it) }
        results[ISO_EURO]?.let { euro.setText(it) }
        results[ISO_POUND]?.let { pound.setText(it) }

        setUpListeners()
    }

    class TextWatcherWow(private val afterTextChanged: (String) -> Unit) : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            if (editable.isNullOrBlank()) {
                afterTextChanged.invoke(ZERO)
            } else {
                afterTextChanged.invoke(editable.toString())
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.dispose()
    }

    companion object {
        private const val ISO_DOLLAR = "USD"
        private const val ISO_EURO = "EUR"
        private const val ISO_POUND = "GBP"
        private const val ZERO = "0"
    }
}