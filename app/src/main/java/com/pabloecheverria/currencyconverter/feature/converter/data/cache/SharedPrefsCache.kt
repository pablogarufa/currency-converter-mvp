package com.pabloecheverria.currencyconverter.feature.converter.data.cache

import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pabloecheverria.currencyconverter.core.cache.Cache
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates
import java.lang.reflect.Type

class SharedPrefsCache(private val sharedPreferences: SharedPreferences, private val gson: Gson) :
    Cache<String, Rates> {

    override fun get(baseCurrency: String): Rates? {
        val json = sharedPreferences.getString(baseCurrency, null)
        val typeOfHashMap: Type = object : TypeToken<Map<String, Double>>() {}.type
        return json?.let { Rates(baseCurrency, gson.fromJson(json, typeOfHashMap)) }
    }

    override fun set(baseCurrency: String, rates: Rates) {
        val ratesString = gson.toJson(rates)
        val editor = sharedPreferences.edit()
        editor.putString(baseCurrency, ratesString)
        editor.apply()
    }
}