package com.pabloecheverria.currencyconverter.feature.converter.data.net

import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

internal interface CurrencyApi {

    @GET("latest")
    fun currencies(@Query("base") baseCurrency: String): Call<RatesDTO>
}