package com.pabloecheverria.currencyconverter.feature.converter

import android.app.Application
import com.pabloecheverria.currencyconverter.core.di.currenciesModule
import com.pabloecheverria.currencyconverter.core.di.networkModule
import com.pabloecheverria.currencyconverter.core.di.repositoryModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyConverterApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CurrencyConverterApplication)
            modules(currenciesModule, networkModule, repositoryModule)
        }
    }
}