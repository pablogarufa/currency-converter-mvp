package com.pabloecheverria.currencyconverter.feature.converter.data.entity

import com.google.gson.annotations.SerializedName

data class RatesDTO(
    @SerializedName("base") val base: String,
    @SerializedName("date") val date: String,
    @SerializedName("rates") val rates: HashMap<String, Double>
)