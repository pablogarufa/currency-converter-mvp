package com.pabloecheverria.currencyconverter.feature.converter.data.cache

import com.pabloecheverria.currencyconverter.core.cache.Cache
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates

class MemoryCache(private val cache: HashMap<String, Rates>) : Cache<String, Rates> {

    override fun get(baseCurrency: String): Rates? {
        return cache[baseCurrency]
    }

    override fun set(baseCurrency: String, rates: Rates) {
        cache[baseCurrency] = rates
    }
}