package com.pabloecheverria.currencyconverter.feature.converter.data

import com.pabloecheverria.currencyconverter.core.mapper.Mapper
import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates

class RatesMapper : Mapper<RatesDTO, Rates> {

    override fun map(input: RatesDTO): Rates {
        return Rates(input.base, input.rates)
    }
}