package com.pabloecheverria.currencyconverter

import android.app.Application
import com.pabloecheverria.currencyconverter.core.di.currenciesModule
import com.pabloecheverria.currencyconverter.core.di.repositoryModule
import com.pabloecheverria.currencyconverter.core.di.testNetworkModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class CurrencyTestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@CurrencyTestApplication)
            modules(currenciesModule, testNetworkModule, repositoryModule)
        }
    }
}