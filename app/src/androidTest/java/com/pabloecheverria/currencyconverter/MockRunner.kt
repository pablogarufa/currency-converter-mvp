package com.pabloecheverria.currencyconverter


import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner

class MockRunner : AndroidJUnitRunner() {

    @Throws(
        InstantiationException::class,
        IllegalAccessException::class,
        ClassNotFoundException::class
    )
    override fun newApplication(
        classLoader: ClassLoader,
        className: String,
        context: Context
    ): Application =
        super.newApplication(classLoader, CurrencyTestApplication::class.java.name, context)
}