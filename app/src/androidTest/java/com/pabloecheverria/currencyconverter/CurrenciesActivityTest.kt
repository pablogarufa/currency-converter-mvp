package com.pabloecheverria.currencyconverter

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.pabloecheverria.currencyconverter.feature.converter.presentation.CurrenciesActivity
import kotlinx.coroutines.test.runBlockingTest
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CurrenciesActivityTest {

    private lateinit var mockWebServer: MockWebServer

    @Rule
    @JvmField
    var popularShowsRule: ActivityTestRule<CurrenciesActivity> =
        ActivityTestRule(CurrenciesActivity::class.java)

    @Before
    fun setUp() {
        mockWebServer = MockWebServer()
        mockWebServer.start(8080)
    }

    @Test
    fun `whenUserInputValueTheOtherCurrenciesAreUpdated`() = runBlockingTest {

        val dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse =
                MockResponse().setResponseCode(404) // .setBody("json/response_currencies_rates_ok.json")
        }

        mockWebServer.setDispatcher(dispatcher)

        onView(withId(R.id.dollar)).perform(replaceText("1"))
        onView(withId(R.id.euro)).check(matches(withText("0")))
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }
}
