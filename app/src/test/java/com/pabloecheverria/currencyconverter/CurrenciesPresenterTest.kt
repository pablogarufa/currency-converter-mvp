package com.pabloecheverria.currencyconverter

import arrow.core.Left
import arrow.core.Right
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.pabloecheverria.currencyconverter.core.coroutines.AsyncExecutor
import com.pabloecheverria.currencyconverter.core.extension.format
import com.pabloecheverria.currencyconverter.feature.converter.domain.ConvertCurrency
import com.pabloecheverria.currencyconverter.feature.converter.domain.DomainError.GeneralError
import com.pabloecheverria.currencyconverter.feature.converter.domain.GetCurrencyRates
import com.pabloecheverria.currencyconverter.feature.converter.presentation.CurrenciesPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Test

class CurrenciesPresenterTest {

    private val view: CurrenciesPresenter.View = mock()
    private val getCurrencyRates: GetCurrencyRates = mock()
    private val convertCurrency: ConvertCurrency = mock()
    private val testDispatcher = TestCoroutineDispatcher()

    lateinit var asyncExecutor: AsyncExecutor
    lateinit var currenciesPresenter: CurrenciesPresenter

    @Before
    fun setup() {
        Dispatchers.setMain(testDispatcher)
        asyncExecutor = AsyncExecutor(testDispatcher)
        currenciesPresenter =
            CurrenciesPresenter(view, asyncExecutor, getCurrencyRates, convertCurrency)
    }

    @Test
    fun `WHEN user request currency rates THEN retrieves an error`() {
        whenever(getCurrencyRates.invoke(baseCurrency, amount)) doReturn Left(GeneralError)

        currenciesPresenter.convertAll(baseCurrency, amount)

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showGeneralError()
    }

    @Test
    fun `WHEN user request currency rates THEN retrieve list of currency rates`() {
        val map: Map<String, Double> = mapOf(Pair("EUR", 99.0), Pair("GBP", 85.1))
        whenever(getCurrencyRates.invoke(baseCurrency, amount)) doReturn Right(map)

        currenciesPresenter.convertAll(baseCurrency, amount)

        verify(view).showLoading()
        verify(view).hideLoading()
        verify(view).showConversion(map.mapValues { it.value.format(1) })
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
        testDispatcher.cleanupTestCoroutines()
    }

    companion object {
        private const val baseCurrency = "USD"
        private const val amount = 100.0
    }
}
