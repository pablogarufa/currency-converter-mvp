package com.pabloecheverria.currencyconverter

import arrow.core.Left
import arrow.core.Right
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.doThrow
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.pabloecheverria.currencyconverter.core.exception.CurrencyError.ServerError
import com.pabloecheverria.currencyconverter.feature.converter.data.net.CurrencyApi
import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import com.pabloecheverria.currencyconverter.feature.converter.data.net.CurrencyService
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import kotlin.test.assertEquals

class CurrencyServiceTest {

    private val response: RatesDTO = mock()
    private val retrofit: Retrofit = mock()
    private val serviceApi: CurrencyApi = mock()
    private val call: Call<RatesDTO> = mock()

    private lateinit var service: CurrencyService

    @Before
    fun setup() {
        whenever(serviceApi.currencies(baseCurrency)) doReturn call
        whenever(retrofit.create(CurrencyApi::class.java)) doReturn serviceApi
        service = CurrencyService(retrofit)
    }

    @Test
    fun `WHEN user request currencies THEN return an exception`() {
        whenever(call.execute()) doThrow IOException()

        assertEquals(Left(ServerError), service.currencies(baseCurrency))
    }

    @Test
    fun `WHEN user request currencies THEN return an error response`() {

        val errorResponse: Response<RatesDTO> = mock {
            on { isSuccessful } doReturn false
        }

        whenever(call.execute()) doReturn errorResponse

        assertEquals(Left(ServerError), service.currencies(baseCurrency))
    }

    @Test
    fun `WHEN user request currencies THEN return a successful response`() {

        val successfulResponse: Response<RatesDTO> = mock {
            on { isSuccessful } doReturn true
            on { body() } doReturn response
        }

        whenever(call.execute()) doReturn successfulResponse
        whenever(successfulResponse.body()) doReturn response

        assertEquals(Right(response), service.currencies(baseCurrency))
    }

    companion object {
        private const val baseCurrency = "USD"
    }
}
