package com.pabloecheverria.currencyconverter

import arrow.core.Left
import arrow.core.Right
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.pabloecheverria.currencyconverter.feature.converter.domain.CurrencyRepository
import com.pabloecheverria.currencyconverter.feature.converter.domain.DomainError.GeneralError
import com.pabloecheverria.currencyconverter.feature.converter.domain.GetCurrencyRates
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class GetCurrencyRatesTest {

    private val currenciesRepository: CurrencyRepository = mock()

    lateinit var getCurrencyRates: GetCurrencyRates

    @Before
    fun setup() {
        getCurrencyRates = GetCurrencyRates(currenciesRepository)
    }

    @Test
    fun `WHEN user request currencies THEN return a server error`() {
        whenever(currenciesRepository.getCurrencyRates(baseCurrency)) doReturn Left(GeneralError)

        assertEquals(Left(GeneralError), getCurrencyRates.invoke(baseCurrency, amount))
    }

    @Test
    fun `WHEN user request currencies THEN return an empty list`() {
        whenever(currenciesRepository.getCurrencyRates(baseCurrency)) doReturn Right(
            Rates(baseCurrency, emptyMap())
        )

        assertEquals(Right(emptyMap()), getCurrencyRates.invoke(baseCurrency, amount))
    }

    @Test
    fun `WHEN user request currencies THEN return a list`() {

        val results = Rates(baseCurrency, mapOf(Pair("EUR", 0.9), Pair("GBR", 0.8)))
        val expected = Rates(baseCurrency, mapOf(Pair("EUR", 90.0), Pair("GBR", 80.0)))

        whenever(currenciesRepository.getCurrencyRates(baseCurrency)) doReturn Right(results)

        assertEquals(Right(expected.rates), getCurrencyRates.invoke(baseCurrency, amount))
    }

    companion object {
        private const val baseCurrency = "USD"
        private const val amount = 100.0
    }
}
