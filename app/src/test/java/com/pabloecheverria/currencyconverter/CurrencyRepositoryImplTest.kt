package com.pabloecheverria.currencyconverter

import arrow.core.Left
import arrow.core.Right
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.pabloecheverria.currencyconverter.core.cache.Cache
import com.pabloecheverria.currencyconverter.core.exception.CurrencyError
import com.pabloecheverria.currencyconverter.core.mapper.Mapper
import com.pabloecheverria.currencyconverter.feature.converter.data.CurrencyRepositoryImpl
import com.pabloecheverria.currencyconverter.feature.converter.data.RatesMapper
import com.pabloecheverria.currencyconverter.feature.converter.data.cache.SharedPrefsCache
import com.pabloecheverria.currencyconverter.feature.converter.data.entity.RatesDTO
import com.pabloecheverria.currencyconverter.feature.converter.data.net.CurrencyService
import com.pabloecheverria.currencyconverter.feature.converter.domain.Rates
import org.junit.Before
import org.junit.Test

class CurrencyRepositoryImplTest {

    private val service: CurrencyService = mock()
    private val memoryCache: Cache<String, Rates> = mock()
    private val sharedPrefsCache: Cache<String, Rates> = mock()
    private val ratesMapper: Mapper<RatesDTO, Rates> = mock()

    lateinit var respository: CurrencyRepositoryImpl

    @Before
    fun setup() {
        respository = CurrencyRepositoryImpl(service, memoryCache, sharedPrefsCache, ratesMapper)
    }

    @Test
    fun `WHEN user request currencies THEN return a server error`() {

        //whenever(sharedPrefsCache.getCurrencyRates(baseCurrency)) doReturn null
        whenever(service.currencies(baseCurrency)) doReturn Left(CurrencyError.ServerError)

        respository.getCurrencyRates(baseCurrency)
    }

    companion object {
        private const val baseCurrency = "USD"
    }
}
